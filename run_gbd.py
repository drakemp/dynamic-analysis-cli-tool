import subprocess as sp
import argparse as ap
from itertools import zip_longest

# prints module for colors. VERBOSE=true if output from print_msg
from prints import print_err, print_msg, print_succ, prints_deinit, prints_init

def main():
    global BIN
    global ARGS
    global SKIP_RUN

    ### RUN GDB
    # yes this is mostly bash so far, but that changes
    if not SKIP_RUN:
        # Remove old gdb log
        # sp.run(['rm', 'gdb.txt'])
        entry = sp.run(['gdb', '-q', '--batch', '-ex', 'info files', BIN],
                       stdout=sp.PIPE)

        # If this is not formatted correctly something went wrong
        try:
            entry = (str(entry.stdout)).split('\\n\\t')[2].split(' ')[2]
        except:
            print_err('Error getting entry point with gdb')
            exit(1)

        # using the entry point, run the binary with the 'execute' instructions
        print_msg('Entry point found: {}'.format(entry))
        print_msg("Executing binary...")
        entry = sp.run(['gdb', '-q', '--batch', '-ex', 'b *'+entry,
                        '-x', 'gdb_input/execute' , BIN])
        # remove the 'Breakpoint 1, ' and '1: x/i $pc' for uniformity in the parser
        sp.run(['sed', '-i', '1,2s/Breakpoint 1, //', 'gdb.txt'])
        sp.run(['sed', '-i', '/1: x\/i $pc/d', 'gdb.txt'])
        print_succ("Done executing Binary")
    else:
        print_msg("Skipping gdb binary execution")
        
    ### PARSE OUTPUT INTO DATABASE
    
    with open('gdb.txt', 'r') as file:
        t = 0 #time t
        file.readline() #junk
        for lines in zip_longest(*[file] * 18):
            instruction = parse_instruction(lines) #return dictionary
            print(instruction)
            t+=1
            if t == 3:
                break# only one at a time for debugging
    
def parse_instruction(block):
    dict = {}
    line0 = block[0].split()
    dict['vaddr'] = line0[0]
    dict['func'] = line0[2]
    return dict

        
# Runs before main
def init():
    global BIN
    global ARGS
    global VERBOSE
    global SKIP_RUN

    parser = ap.ArgumentParser(description=
            "This program takes a binary and runs in gdb to output data on execution")
    parser.add_argument('-f', metavar='FILE', type=str, required=True,
                        help="Binary to be ran")
    parser.add_argument('-a', metavar='ARGS', type=str,
                        help="Arguments to the binary, must be supplied in quotes!")
    parser.add_argument('-v', action='store_true',
                        help="More print messages (blue)")
    parser.add_argument('--skip_run', action='store_true', default=False,
                        help="Skip running the binary and parse the existing output")
    args = parser.parse_args()
    args = vars(args)

    BIN = args['f']
    ARGS = args['a']
    VERBOSE = args['v']
    SKIP_RUN = args['skip_run']
    
    prints_init(VERBOSE)
    

###_start###    
init()
try:
    main()
except Exception as e:
    print(e)

finally:
    prints_deinit()
