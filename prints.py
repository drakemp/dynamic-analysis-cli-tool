import colorama as color
import sys

def prints_init(v):
    global VERBOSE 
    VERBOSE = v
    color.init()

def prints_deinit():
    color.deinit()

def print_msg(msg):
    global VERBOSE
    if VERBOSE:
        print(color.Fore.BLUE + '[-]' + color.Style.RESET_ALL + ' ' + msg)

def print_err(msg):
    print((color.Fore.RED + '[!]' + color.Style.RESET_ALL + ' ' + msg), file=sys.stderr)

def print_succ(msg):
    print(color.Fore.GREEN + '[*]' + color.Style.RESET_ALL + ' ' + msg)

